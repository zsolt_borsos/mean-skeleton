var express = require('express');

//set environment
var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
//create app
var app = express();

//load config
var config = require('./server/config/config')[env];
//load express settings
require('./server/config/express')(app, config);
//load mongoose settings
require('./server/config/mongoose')(config);

require('./server/config/passport')();


//load routes
require('./server/config/routes')(app);

//start server
app.listen(config.port);

console.log('Listening on port: ' + config.port );
